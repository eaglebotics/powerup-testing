package org.usfirst.frc.team5410.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/*
 * A class to rotate a single motor 43 times. Requested by Matthew.
 */

public class Rotate43 extends Command
{
	private WPI_TalonSRX motorController;


	/*
	 * 1.	Constructor - Might have parameters for this command such as target positions of devices. Should also set the name of the command for debugging purposes.
	 *  This will be used if the status is viewed in the dashboard. And the command should require (reserve) any devices is might use.
	 */
    public Rotate43(WPI_TalonSRX motorController2) {
    	super("Rotate43");
    	motorController = motorController2;
    	motorController.setSelectedSensorPosition(0, 0, 10000);
		
    }

    // 	initialize() - This method sets up the command and is called immediately before the command is executed for the first time and every subsequent time it is started .
    //  Any initialization code should be here. 
    protected void initialize() {
    }

    /*
     *	execute() - This method is called periodically (about every 20ms) and does the work of the command. Sometimes, if there is a position a
     *  subsystem is moving to, the command might set the target position for the subsystem in initialize() and have an empty execute() method.
     */
    protected void execute() {
		motorController.set(0.1);
		SmartDashboard.putNumber("Encoder#", motorController.getSelectedSensorPosition(0));
		
    	if (motorController.getSelectedSensorPosition(0) >= (80 * 43)) {
    		motorController.set(0);
    	}
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
    	if (Robot.timer.get() >= 15) {
    		Robot.timer.stop();
    		Robot.timer.reset();
    		return true;
    	}
    	return false;
    }

}
